/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/16 23:31:36 by juepee-m          #+#    #+#             */
/*   Updated: 2018/04/20 17:28:35 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *haystack, const char *needle, size_t len)
{
	const char	*hay;
	const char	*nee;
	size_t		size;

	if (!*needle)
		return ((char *)haystack);
	while (*haystack && len--)
	{
		if (*haystack == *needle)
		{
			hay = haystack + 1;
			nee = needle + 1;
			size = len;
			while (*nee && *hay && *hay == *nee && size--)
			{
				hay++;
				nee++;
			}
			if (!*nee)
				return ((char *)haystack);
		}
		haystack++;
	}
	return (NULL);
}
