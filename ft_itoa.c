/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/13 20:48:53 by juepee-m          #+#    #+#             */
/*   Updated: 2018/04/20 22:46:13 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static void	is_negative(long *nb, int *negative)
{
	if (*nb < 0)
	{
		*nb *= -1;
		*negative = 1;
	}
}

char		*ft_itoa(int n)
{
	long	nb;
	int		len;
	int		negative;
	char	*str;

	nb = n;
	len = 2;
	negative = 0;
	while ((n = n / 10))
		len++;
	is_negative(&nb, &negative);
	len += negative;
	if (!(str = (char *)malloc(sizeof(char) * len)))
		return (NULL);
	str[--len] = '\0';
	while (len--)
	{
		str[len] = (nb % 10) + 48;
		nb = nb / 10;
	}
	if (negative)
		str[0] = '-';
	return (str);
}
