/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/13 21:08:19 by juepee-m          #+#    #+#             */
/*   Updated: 2018/04/13 21:08:28 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *str, const char *to_find)
{
	const char *cpy_str;
	const char *cpy_to_find;

	if (*to_find == '\0')
		return ((char *)str);
	while (*str)
	{
		cpy_str = str;
		cpy_to_find = to_find;
		while (*cpy_to_find && *cpy_str == *cpy_to_find)
		{
			cpy_str++;
			cpy_to_find++;
		}
		if (*cpy_to_find == '\0')
			return ((char *)str);
		str++;
	}
	return (NULL);
}
