/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/13 20:52:54 by juepee-m          #+#    #+#             */
/*   Updated: 2018/06/12 17:00:16 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "libft.h"

void				*ft_memchr(const void *s, int c, size_t n)
{
	const char		*ts;
	unsigned char	u;

	u = (unsigned char)c;
	ts = s;
	while (n--)
	{
		if ((unsigned char)*ts == u)
		{
			s = ts;
			return ((void *)s);
		}
		ts++;
	}
	return (NULL);
}
