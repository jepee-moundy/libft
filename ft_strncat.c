/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/13 21:05:04 by juepee-m          #+#    #+#             */
/*   Updated: 2018/04/13 22:00:06 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncat(char *dest, const char *src, size_t n)
{
	char	*str;

	str = dest;
	while (*str != '\0')
	{
		str++;
	}
	while (*src != '\0' && n > 0)
	{
		*str = *src;
		str++;
		src++;
		n--;
	}
	*str = '\0';
	return (dest);
}
