/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstsize.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/12 18:15:05 by juepee-m          #+#    #+#             */
/*   Updated: 2018/06/12 18:19:25 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t			ft_lstsize(t_list *list)
{
	size_t		len;
	t_list		*tmp;

	len = 0;
	tmp = list;
	while (tmp)
	{
		tmp = tmp->next;
		len++;
	}
	return (len);
}
