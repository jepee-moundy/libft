/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/13 20:51:11 by juepee-m          #+#    #+#             */
/*   Updated: 2018/04/16 22:56:40 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include <stdlib.h>
#include "libft.h"

void	*ft_memalloc(size_t size)
{
	char	*alloc;

	if (!(alloc = (char *)malloc(sizeof(char) * size)))
		return (NULL);
	ft_bzero(alloc, size);
	return ((void *)alloc);
}
