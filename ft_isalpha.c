/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isalpha.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/13 20:43:39 by juepee-m          #+#    #+#             */
/*   Updated: 2018/04/13 20:47:18 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

#define IS_ALPHA(c) ((c >= 65 && c <= 90) || (c >= 97 && c <= 122))

int		ft_isalpha(int c)
{
	if (IS_ALPHA(c))
		return (1);
	return (0);
}
