/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/13 21:07:38 by juepee-m          #+#    #+#             */
/*   Updated: 2018/04/20 16:49:33 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	char	*begin;

	begin = (char *)s;
	while (*s)
		s++;
	while (*s != *begin && *s != c)
		s--;
	return (*s == c ? (char *)s : NULL);
}
