/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_bits.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/12 17:26:46 by juepee-m          #+#    #+#             */
/*   Updated: 2018/06/12 18:24:17 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "libft.h"

void	print_bits(unsigned char octet)
{
	unsigned char i;

	i = 128;
	while (i >>= 1 != 0)
	{
		if (i & octet)
			write(1, "1", 1);
		else
			write(1, "0", 1);
	}
}
