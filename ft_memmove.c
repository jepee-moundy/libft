/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/13 20:53:49 by juepee-m          #+#    #+#             */
/*   Updated: 2018/04/23 21:39:53 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include <stdlib.h>
#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	unsigned char	*tsrc;
	unsigned char	*tdst;
	size_t			i;

	i = 0;
	tsrc = (unsigned char *)src;
	tdst = (unsigned char *)dst;
	if (tsrc < tdst)
	{
		while (len--)
			*(tdst + len) = *(tsrc + len);
	}
	else
	{
		while (i < len)
		{
			tdst[i] = tsrc[i];
			i++;
		}
	}
	return (dst);
}
