/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/12 17:27:03 by juepee-m          #+#    #+#             */
/*   Updated: 2018/06/12 18:22:08 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "libft.h"

static char		index_base(char *str, int len)
{
	int		i;

	i = 0;
	while (str[i] && i < len)
		i++;
	return (str[i]);
}

void			ft_putnbr_base(int nbr, char *base)
{
	long	nb;
	int		len_base;

	nb = nbr;
	len_base = ft_strlen(base);
	if (nb < 0)
	{
		nb = -nb;
		ft_putchar('-');
	}
	if (nb >= len_base)
	{
		ft_putnbr_base(nb / len_base, base);
		ft_putnbr_base(nb % len_base, base);
	}
	else
		ft_putchar(index_base(base, nb));
}
