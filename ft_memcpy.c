/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/13 20:53:27 by juepee-m          #+#    #+#             */
/*   Updated: 2018/04/23 21:37:45 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "libft.h"

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	char		*cpy_dst;
	const char	*cpy_src;

	cpy_dst = dst;
	cpy_src = src;
	while (n--)
	{
		*cpy_dst = *cpy_src;
		cpy_dst++;
		cpy_src++;
	}
	return (dst);
}
