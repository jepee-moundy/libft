/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/13 20:51:55 by juepee-m          #+#    #+#             */
/*   Updated: 2018/06/12 17:06:00 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include "libft.h"

void		*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	const char	*tsrc;
	char		*tdst;
	size_t		i;

	i = 0;
	tsrc = src;
	tdst = dst;
	while (i < n)
	{
		*tdst = *tsrc;
		if ((unsigned char)*tsrc == (unsigned char)c)
			return ((void *)tdst + 1);
		tdst++;
		tsrc++;
		n--;
	}
	return (NULL);
}
